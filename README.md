# Symulacja Układu Słonecznego

Projekt symulacji układu słonecznego

## Instalacja

Aby zainstalować niezbędne paczki, wykonaj następujące kroki:
1. Przejdź do głównego katalogu projektu.
2. Uruchom środowisko Julia.
3. Wprowadź poniższe komendy:
```bash
julia
]
instantiate
```
W tym momencie zainstalują się wszystkie niezbędne pakiety, następnie możesz uruchomić projekt.<br>
Jeśli ten etap nie został wykonany poprawnie i dalej nie możesz uruchomić projektu, najlepiej będzie zacząć od manualnego dodania każdej paczki znajdującej się w `Project.toml [deps]`. Przykładowo:
```bash
julia
]
add Makie 
add GLMakie
(...)
```

## Obsługa

Aby poprawnie korzystać z aplikacji, postępuj zgodnie z poniższymi wskazówkami:
1. Uruchom aplikację w trybie pełnoekranowym, najlepiej przy rozdzielczości 1920x1080.
2. Na dole aplikacji znajduje się przycisk Włącz/Wyłącz, który służy do sterowania symulacją.
3. Po prawej stronie aplikacji, mamy sidebar z ustawieniami projektu, możemy tam:
* Zmienić wartość "delta time" - tzn. jak szybko mają "uciekać" dni (przesunięty wskaźnik na lewo to szybciej, na prawo to wolniej). Prędkość również zależy od mocy obliczeniowej komputera
* Entity+mass - po wybraniu w selectcie planety możemy zmienić masę planety. Ważne, aby nową wartość potwierdzić klawiszem enter.