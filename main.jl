using GLMakie
GLMakie.activate!()

include("src/models/astro_entity.jl")
include("src/data/constants.jl")
include("src/utils/calculations.jl")
include("src/utils/data.jl")
include("src/utils/visualization.jl")

function add_entity_to_galaxy!(galaxy::Dict{String, AstroEntity}, charts, entity::AstroEntity, color)
    push!(galaxy, entity.name => entity)

    # Użyta skala logarytmiczna, żeby na ekranie zmieściło się
    # zarówno słońce, jak i malutki merkury.
    log_radius = log10(entity.radius)
    marker_size = 18 * 2^(log_radius - log10(696340))

    # Narysowanie planety. Widok 3D
    meshscatter!(charts[1], galaxy[entity.name].position_normal_full, color=color, markersize=marker_size, label=entity.name)
    # ...i jej orbity
    meshscatter!(charts[1], galaxy[entity.name].position_normal_full_history, color=color, markersize=0.4, label=entity.name, axis = (; type = Axis3, protrusions = (0, 0, 0, 0), viewmode = :fit))

    # Narysowanie planety. Widok z boku
    scatter!(charts[2], galaxy[entity.name].position_normal_front, color=color, markersize=marker_size * 2.8, label=entity.name)

    # Narysowanie planety. Widok z góry
    scatter!(charts[3], galaxy[entity.name].position_normal_full, color=color, markersize=marker_size * 2.8, label=entity.name)
    # ...i jej orbity
    lines!(charts[3], galaxy[entity.name].position_normal_full_history, color=color) 
end

function main()
    t = Δt

    galaxy = Dict{String, AstroEntity}()
    entities = generate_entities_from_file("./src/data/entities.json")

    figure, chart_layout, controls_layout, settings_layout, charts = create_layout()

    for entity in entities
        add_entity_to_galaxy!(
            galaxy,
            charts,
            entity,
            entity.color,
        )
    end

    # Util do generowania legendy
    entity_colors = [PolyElement(color = entity.color, strokecolor = :transparent) for (_, entity) in galaxy]
    entity_names = [entity.name for (_, entity) in galaxy]

    # Budowanie layoutu
    play_pause_button = create_controls(controls_layout)
    time_label, time_slider, settings_entity_name, settings_entity_mass_input, focus_cam_select  = create_settings(settings_layout, entity_names)

    settings_layout[1, 1:2] = Legend(figure, entity_colors, entity_names, "Entities", framevisible = false, margin=(5,5,5,5), tellwidth=false, tellheight=false, nbanks=2)

    # Główne zmienne odnośnie przeprowadzania symulacji
    is_running = Observable(false)
    refresh_time = time_slider.value
    frame = 1

    # Wybrane domyślne ustawienia
    selected_entity::AstroEntity = galaxy["sun"]
    settings_entity_name.selection = "sun"
    focused_entity::AstroEntity = galaxy["sun"]

    # Listeners:
    ## Włącz/Wyłącz symulację
    on(play_pause_button.clicks) do clicks
        is_running[] = !is_running[]
    end

    ## Ustaw inną planetę do edycji masy
    on(settings_entity_name.selection) do select
        selected_entity = galaxy[select]
        mass = selected_entity.mass[]
        settings_entity_mass_input.displayed_string[] = "$mass"
    end

    ## Ustaw inną planetę do śledzenia przez kamery
    on(focus_cam_select.selection) do select
        focused_entity = galaxy[select]
    end

    ## Zmień masę planety
    on(settings_entity_mass_input.stored_string) do text
        mass_float::Float64 = parse(Float64, text)
        selected_entity.mass[] = mass_float
    end

    ## Główna pętla symulacji
    on(play_pause_button.clicks) do clicks
        @async while is_running[]
            isopen(figure.scene) || break # ensures computations stop if closed window

            # Cała 'matematyka'
            galaxy_move!(galaxy)
            t += Δt

            # Kalkuacje czysto wizualne. Ruszanie wykresen 3D po krzywej.
            charts[1].azimuth[] = 2.21pi + 0.2 * sin(2pi * frame / 480)
            charts[1].elevation[] = 0.26 + 0.1 * sin(2pi * frame / 480)
            frame += 1

            center_on_entity!(focused_entity, charts)

            passed_days::Int = floor(t / SECONDS_PER_DAY) % 365
            passed_years::Int = floor(t / SECONDS_PER_DAY / 365) + 1
            time_label.text = "Year: $passed_years Day: $passed_days"

            sleep(refresh_time[])
        end
    end

    display(figure)
end

main()