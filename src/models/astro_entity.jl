mutable struct AstroEntity
    name::String
    mass::Observable{Float64}
    position::Vector{Float64}
    velocity::Vector{Float64}
    color::Symbol
    radius::Float64

    position_normal_full::Observable{Point3f0}
    position_normal_full_history::Observable{Vector{Point3f0}}

    position_normal_front::Observable{Point2f0}
    position_normal_front_history::Observable{Vector{Point2f0}}

    function AstroEntity(name, mass, position, velocity, color, radius)
        this = new(name, Observable(mass), position, velocity, color, radius, Observable(Point3f0(0, 0, 0)), Observable([]), Observable(Point2f0(0, 0)), Observable([]))
        normalize_position!(this)
        return this
    end
end

function normalize_position!(cosmic_object::AstroEntity)
    # Znormalizowana pozycja
    x = round(cosmic_object.position[1] / ASTRONOMICAL_UNIT * 50, digits=2)
    y = round(cosmic_object.position[2] / ASTRONOMICAL_UNIT * 50, digits=2)  
    z = round(cosmic_object.position[3] / ASTRONOMICAL_UNIT * 50, digits=2)  

    # Gromadzimy to jako Observable, żeby wykresy dynaczminie się przeładowywały
    cosmic_object.position_normal_full[] = Point3f0(x, y, z)
    cosmic_object.position_normal_front[] = Point2f0(x, z)

    # Orbita lotu planety, przechowywana w pamięci ostatnie 180 wartości.
    pushfirst!(cosmic_object.position_normal_full_history[], Point3f0(x, y, z))
    pushfirst!(cosmic_object.position_normal_front_history[], Point2f0(x, z))
    if length(cosmic_object.position_normal_front_history[]) > 180
        pop!(cosmic_object.position_normal_full_history[])
        pop!(cosmic_object.position_normal_front_history[])
    end

    # Dokonujemy zmianny na tablicach, które są Observable
    # - więc wymagane jest poinformowanie aplikacji:
    notify(cosmic_object.position_normal_full_history)
    notify(cosmic_object.position_normal_front_history)
end