include("./../data/constants.jl")
include("./../models/astro_entity.jl")

function update_velocity!(entity1::AstroEntity, entity2::AstroEntity)
    # Obliczanie odległości na podstawie twierdzenia Pitagorasa
    distance_vector = entity1.position .- entity2.position
    # Skalar wektora odległości
    distance = sqrt(distance_vector[1]^2 + distance_vector[2]^2 + distance_vector[3]^2)

    # Pomocniczne obliczanie wielkości 'siły' jako (-GMm)/r_skalar^2 
    force_magnitude = (-1 * GRAVITATIONAL_CONSTANT * entity1.mass[] * entity2.mass[]) / distance^2
    # Obliczanie 'siły' z wzoru: F = (-GMm)r_vector/(r_skalar^2 * r_vector)
    # Z pomocą wyliczonej samej wartości siły mamy F =
    force = force_magnitude .* (distance_vector / distance)

    # Zaktualizowanie aktulanej prędkości z zachowaniem mnożenia przez -1,
    # bo wektory działają na siebie przeciwną siłą.
    # a = F/m ; delta_v = a * delta_t. Zatem delta_v = (F * delta_t) / m 
    entity1.velocity += (force * Δt) / entity1.mass[]
    entity2.velocity += (-1.0 * force * Δt) / entity2.mass[]
end

function update_position!(entity::AstroEntity)
    entity.position += entity.velocity * Δt
    normalize_position!(entity)
end

function galaxy_move!(galaxy::Dict{String, AstroEntity})
    galaxy_entities = collect(values(galaxy))

    for i in 1:length(galaxy)
        for j in i:length(galaxy)
            # Nie liczymy działania planety samej na siebie. 
            if i == j
                continue
            end

            entity1 = galaxy_entities[i]
            entity2 = galaxy_entities[j]

            update_velocity!(entity1, entity2)
        end
    end

    for entity in values(galaxy)
        update_position!(entity)
    end
end
