function create_layout()
    set_theme!(theme_black())

    f = Figure(fontsize=16, backgroundcolor = RGBf0(0.11, 0.11, 0.11))

    chart_layout = f[2, 1:2] = GridLayout()
    controls_layout = f[3, 1:2] = GridLayout()
    settings_layout = f[2, 3] = GridLayout()

    Label(f[1, :], text = "Solar system", tellwidth = false, tellheight = false, fontsize = 50)
    Label(f[3, 3], "Pakiety mst.", tellwidth = false, tellheight = false)

    colsize!(f.layout, 3, Relative(1/5))
    colgap!(f.layout, 25)

    rowsize!(f.layout, 1, Relative(1/7))
    rowsize!(f.layout, 3, Relative(1/7))

    ax1 = Axis3(chart_layout[1:2, 1],
        xlabel = "X",
        ylabel = "Y",
        zlabel = "Z",
        aspect = :data,
        elevation = 0.26,
        azimuth = 2.21pi,
        perspectiveness = 0.25,
        padding=(0,0,0,0),
    )

    xlims!(ax1, -100, 100)
    ylims!(ax1, -100, 100)
    zlims!(ax1, -50, 50)

    ax2 = Axis(chart_layout[1, 2],
        ylabel = "Z",
        aspect = 2,
    )

    hidexdecorations!(ax2, grid = false)

    xlims!(ax2, -100, 100)
    ylims!(ax2, -50, 50)

    ax3 = Axis(chart_layout[2, 2],
        ylabel = "Y",
        xlabel = "X",
        aspect = 1,
    )

    ax3.tellheight = true

    xlims!(ax3, -100, 100)
    ylims!(ax3, -100, 100)

    rowsize!(chart_layout, 1, Relative(1/3))
    colsize!(chart_layout, 2, Relative(1/4))
    rowgap!(chart_layout, 25)

    charts = [ax1, ax2, ax3]

    return f, chart_layout, controls_layout, settings_layout, charts
end

function center_on_entity!(entity::AstroEntity, charts)
    limits_xz = charts[2].targetlimits[]
    limits_xy = charts[3].targetlimits[]

    distx = floor(limits_xy.widths[1] / 2)
    disty = floor(limits_xy.widths[2] / 2)
    distz = floor(limits_xz.widths[2] / 2)

    x_center = entity.position_normal_full[][1]
    y_center = entity.position_normal_full[][2]
    z_center = entity.position_normal_full[][3]
    
    xlims!(charts[2], x_center - disty, x_center + disty)
    ylims!(charts[2], z_center - distz, z_center + distz)

    xlims!(charts[3], x_center - disty, x_center + disty)
    ylims!(charts[3], y_center - disty, y_center + disty)
end

function create_controls(layout::GridLayout)
    play_pause_button = Button(layout[1, 1]; label = "Play/Pause", tellwidth = false, buttoncolor=RGBf0(255, 255, 255), labelcolor=RGBf0(0, 0, 0), halign=:left)

    return play_pause_button
end

function create_settings(layout::GridLayout, entities::Vector{String})
    time_label = Label(layout[2, 1:2], text="Day: 1", fontsize=30, tellwidth = false, tellheight = false, justification = :center, valign=:top)

    time_slider_lbl = Label(layout[3, 1], text="Delta time:", tellwidth = false, tellheight = false, justification = :center, valign=:top)
    time_slider = Slider(layout[3, 2], range = [0.00000005, 0.000001, 0.0005, 0.0001, 0.005, 0.001, 0.01, 0.05], horizontal = true, startvalue = 0.005, valign=:top, tellheight = false)

    entity_select_lbl = Label(layout[4, 1], text="Entity:", fontsize=30, tellwidth = false, tellheight = false, justification = :center)
    entity_select = Menu(layout[4, 2], options = entities, tellwidth = false, tellheight = false, justification = :center)

    entity_mass_lbl = Label(layout[5, 1], text="Mass:", fontsize=30, tellwidth = false, tellheight = false, justification = :center)
    entity_mass = Textbox(layout[5, 2], validator = Float64, tellwidth = false, tellheight = false, justification = :center, width=170)

    focus_cam_lbl = Label(layout[6, 1], text="Focus:", fontsize=30, tellwidth = false, tellheight = false, justification = :center)
    focus_cam = Menu(layout[6, 2], options = entities, tellwidth = false, tellheight = false, justification = :center)

    return time_label, time_slider, entity_select, entity_mass, focus_cam
end

