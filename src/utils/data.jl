using JSON

include("./../data/constants.jl")
include("./../models/astro_entity.jl")

function generate_entities_from_file(filename::AbstractString)::Vector{AstroEntity}
    entities = Vector{AstroEntity}()

    json_data = JSON.parsefile(filename)

    for (name, data) in json_data
        mass = data["mass"]
        position = data["position"]
        velocity = data["velocity"]
        color = Symbol(data["color"])
        radius = data["radius"]

        entity = AstroEntity(
            name,
            mass,
            [position["x"] * ASTRONOMICAL_UNIT, position["y"] * ASTRONOMICAL_UNIT, position["z"] * ASTRONOMICAL_UNIT],
            [velocity["x"], velocity["y"], velocity["z"]],
            color,
            radius
        )

        push!(entities, entity)
    end

    return entities
end
