const GRAVITATIONAL_CONSTANT = 6.674e-11 # https://en.wikipedia.org/wiki/Gravitational_constant 
const ASTRONOMICAL_UNIT = 149597870700 # https://en.wikipedia.org/wiki/Astronomical_unit 
const SECONDS_PER_DAY = 60 * 60 * 24
const Δt = 1 * SECONDS_PER_DAY